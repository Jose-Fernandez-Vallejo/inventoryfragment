package com.jafernandez.inventory.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class Dependency implements Parcelable
{

    public static final String TAG = "dependency" ;
    private String name;
    private String shortname;
    private String description;
    private String inventory;
    private String urlImage;

    public Dependency()
    {

    }
    public Dependency(String name, String shortname, String description, String inventory ,String urlImage) {
        this.name = name;
        this.shortname = shortname;
        this.description = description;
        this.inventory = inventory;
        this.urlImage = urlImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dependency that = (Dependency) o;
        return Objects.equals(shortname, that.shortname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shortname);
    }

    public static final Creator<Dependency> CREATOR = new Creator<Dependency>() {
        @Override
        public Dependency createFromParcel(Parcel in) {
            return new Dependency(in);
        }

        @Override
        public Dependency[] newArray(int size) {
            return new Dependency[size];
        }
    };

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }


    @Override
    public String toString() {
        return name;
    }


    protected Dependency(Parcel in)
    {
        name = in.readString();
        shortname = in.readString();
        description = in.readString();
        inventory = in.readString();
        urlImage = in.readString();
    }

    @Override
    public int describeContents() {
        return CONTENTS_FILE_DESCRIPTOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(shortname);
        dest.writeString(description);
        dest.writeString(inventory);
        dest.writeString(urlImage);
    }
}
