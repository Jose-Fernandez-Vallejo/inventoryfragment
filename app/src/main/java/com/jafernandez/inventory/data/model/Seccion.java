package com.jafernandez.inventory.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class Seccion implements Parcelable {

    public static final String TAG = "Seccion";
    String name;
    String shortName;
    String dependency;
    String description;
    String image;

    public Seccion()
    {

    }
    public Seccion(String name, String shortName, String dependency, String description, String image) {
        this.name = name;
        this.shortName = shortName;
        this.dependency = dependency;
        this.description = description;
        this.image = image;
    }

    protected Seccion(Parcel in) {
        name = in.readString();
        shortName = in.readString();
        dependency = in.readString();
        description = in.readString();
        image = in.readString();
    }

    public static final Creator<Seccion> CREATOR = new Creator<Seccion>() {
        @Override
        public Seccion createFromParcel(Parcel in) {
            return new Seccion(in);
        }

        @Override
        public Seccion[] newArray(int size) {
            return new Seccion[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDependency() {
        return dependency;
    }

    public void setDependency(String dependency) {
        this.dependency = dependency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seccion seccion = (Seccion) o;
        return Objects.equals(shortName, seccion.shortName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shortName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(shortName);
        dest.writeString(dependency);
        dest.writeString(description);
        dest.writeString(image);
    }
}
