package com.jafernandez.inventory.data.repository;

import android.widget.SeekBar;

import com.jafernandez.inventory.data.model.Seccion;

import java.util.ArrayList;
import java.util.List;

public class SeccionRepository {

    private List<Seccion> list;
    private static final SeccionRepository ourInstance = new SeccionRepository();

    public static SeccionRepository getInstance() {
        return ourInstance;
    }

    private SeccionRepository() {
        list =new ArrayList<Seccion>();
        initialice();
    }

    private void initialice() {

    list.add(new Seccion("Fondo norte", "4Fila","3ESO","Estos son lo peligrosos con los que es mejor no meterse", ""));
    list.add(new Seccion("El que no atiende", "Cristobal","1CFGS","Este es el que se tira todo el añomirando el movil y con los auriculares", ""));
    list.add(new Seccion("Primera fila", "1Fila","3ESO","Estas son las ratillas que se ponen delante porque saben que si se ponen atras no atienden", ""));
    list.add(new Seccion("los del medio", "3Fila","3ESO","Estos estan en tierra de nadie y por lo mismo no le importan a nadie", ""));
    }

    public List<Seccion> getList()
    {
        return list;
    };

    public void add(Seccion seccion) {
        list.add(seccion);
    }

    public void edit(Seccion seccion) {
        int pos = list.indexOf(seccion);
        list.remove(pos);
        list.add(pos, seccion);
    }
}
