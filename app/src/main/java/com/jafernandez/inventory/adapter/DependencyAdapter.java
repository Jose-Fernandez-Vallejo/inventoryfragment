package com.jafernandez.inventory.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.jafernandez.inventory.data.model.Dependency;
import com.jafernandez.inventory.data.repository.DependencyRepository;
import com.jafernandez.inventory.R;

import java.util.ArrayList;
import java.util.List;

public class DependencyAdapter extends RecyclerView.Adapter<DependencyAdapter.ViewHolder> {

    private ArrayList<Dependency> list;

    private onItemClickListener listerner;
    private  OnManageDependencyListener listenerm;

    public void load(List<Dependency> dependencyList) {
        Log.d("BORRAR", "cargando");
        list.addAll(dependencyList);
    }

    public void clear() {
        Log.d("BORRAR", "limpia");
        list.clear();
    }

    public void delete(Dependency deleted) {
        list.remove(deleted);
    }

    public void add(Dependency dependency) {
        list.add(dependency);

    }

    public interface OnManageDependencyListener{
        void onEditDependency(Dependency dependency);

        //este sera con la pulsacion larga (onLongClickListener())
        void onDeleteDependency(Dependency dependency);
    }

    public DependencyAdapter() {
        list = new ArrayList();
        //list= (ArrayList)DependencyRepository.getInstance().getList();
    }

    public interface onItemClickListener extends View.OnClickListener
    {
        @Override
        void onClick(View v);
    }

    @NonNull
    @Override
    public DependencyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dependency_item,parent, false);
        view.setOnClickListener(listerner);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DependencyAdapter.ViewHolder holder, int position) {

        holder.icon.setLetter(list.get(position).getName());
        holder.tvName.setText(list.get(position).getName());

        holder.bind(list.get(position),listenerm);


    }
    public void SetOnManageDependency(OnManageDependencyListener listener)
    {
            this.listenerm = listener;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public String getItemName(int position)
    {
        return list.get(position).getName();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        MaterialLetterIcon icon;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon=itemView.findViewById(R.id.icon);
            tvName = itemView.findViewById(R.id.tvName);
        }

        public void bind(final Dependency dependency, final OnManageDependencyListener listener)
        {
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view)
                {
                    listener.onEditDependency(dependency);
                }

            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onDeleteDependency(dependency);
                    return true;
                }
            });

        }
    }
}
