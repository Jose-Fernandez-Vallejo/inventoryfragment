package com.jafernandez.inventory.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jafernandez.inventory.R;
import com.jafernandez.inventory.data.model.Seccion;
import com.jafernandez.inventory.data.repository.SeccionRepository;

import java.util.ArrayList;

public class SeccionAdapter extends RecyclerView.Adapter<SeccionAdapter.ViewHolder> {

    ArrayList<Seccion> list;
    private OnManageSeccionListener listener;
    public SeccionAdapter(OnManageSeccionListener listener)
    {
        this.listener = listener;
        list = (ArrayList<Seccion>)SeccionRepository.getInstance().getList();
    }

    public interface OnManageSeccionListener
    {
        void OnEditSeccion(Seccion seccion);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seccionitemlayout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txvNombreSeccion.setText(list.get(position).getName());

        holder.bind(list.get(position),listener);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txvNombreSeccion;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txvNombreSeccion = itemView.findViewById(R.id.txvNombreSeccion);
        }

        public void bind(final Seccion seccion,final OnManageSeccionListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnEditSeccion(seccion);
                }
            });
        }
    }
}
