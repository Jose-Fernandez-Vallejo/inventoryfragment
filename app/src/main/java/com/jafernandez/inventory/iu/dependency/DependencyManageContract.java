package com.jafernandez.inventory.iu.dependency;


import com.jafernandez.inventory.data.model.Dependency;
import com.jafernandez.inventory.iu.base.BaseView;

/**
 * interfaz que corresponde al contrato que se establece entre DependencyManagerFragment (vista)
 * y DependencyManagePresenter(presenter)*/

public class DependencyManageContract {

    interface View extends BaseView<Presenter>
    {
    void onSuccessValidate();

    }

    interface Presenter
    {
        void ifExists(Dependency dependency);
        void validateDependency(Dependency dependency);
        void add(Dependency dependency);
        void edit(Dependency dependency);
    }
}
