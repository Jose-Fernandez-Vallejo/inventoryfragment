package com.jafernandez.inventory.iu.dependency;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jafernandez.inventory.R;
import com.jafernandez.inventory.adapter.DependencyAdapter;
import com.jafernandez.inventory.data.model.Dependency;
import com.jafernandez.inventory.iu.base.BaseDialogFragment;

import java.util.List;

public class DependencyListFragment extends Fragment implements DependencyListContract.View, BaseDialogFragment.OnFinishDialogListener{


    private static final int CODE_DELETE = 300;
    private FloatingActionButton fab;
    private LinearLayout llProgress;
    private Dependency deleted;
    //objeto-delegado que sirve de comunicacion con activity
    private OnManageDependencyListener listener;
    private DependencyListContract.Presenter presenter;
    //objeto que escucha los eventos del adapter
    private DependencyAdapter.OnManageDependencyListener listenerAdapter;
    private ImageView imgNoData;




    interface OnManageDependencyListener {
        void onManageDependency(Dependency dependency);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener= (OnManageDependencyListener) context;
        }catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString() + "must implepement");
        }
    }

    public static final String TAG = "DependencyListFragment";
    private static final int SPAN_COUNT = 3 ;
    private RecyclerView rvDependency;
    private DependencyAdapter adapter;

    public static Fragment newInstance(Bundle bundle)
    {
        DependencyListFragment fragment = new DependencyListFragment();

        if(bundle!= null)
        {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_dependency_list, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvDependency=view.findViewById(R.id.rvDependency);
        initListDependency();
        fab= view.findViewById(R.id.fabAnadir);
        imgNoData = view.findViewById(R.id.imgNoData);
        llProgress = view.findViewById(R.id.llProgress);
        intfab();
    }

    //inicializa el boton añadir
    private void intfab() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onManageDependency(null);
            }
        });

    }

    //inicializa el RecyclerView
    private void initListDependency() {
        //inicializar listener del adapter
        initListenerAdapter();
        adapter = new DependencyAdapter();
        adapter.SetOnManageDependency(listenerAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvDependency.setLayoutManager(linearLayoutManager);
        rvDependency.setAdapter(adapter);
    }


    //inicializa el listener que esuchca los eventos del adapter
    private void initListenerAdapter()
    {
        listenerAdapter = new DependencyAdapter.OnManageDependencyListener() {
            @Override
            public void onEditDependency(Dependency dependency) {
                listener.onManageDependency(dependency);
            }
            //este metodo muestra el cuadro de dialogo pidiendo confirmacion para eliminar
            @Override
            public void onDeleteDependency(Dependency dependency) {


                showDeleteDialog(dependency);


            }
        };
    }
//muestra el cuadro de dialogo que confirma el delete
    private void showDeleteDialog(Dependency dependency) {
        Bundle bundle = new Bundle();
        bundle.putString(BaseDialogFragment.TITLE,getString(R.string.title_delete));
        bundle.putString(BaseDialogFragment.MESSAGE, getString(R.string.message_delete) + dependency.toString());
        BaseDialogFragment dialogFragment = BaseDialogFragment.newInstance(bundle);
        dialogFragment.setTargetFragment(DependencyListFragment.this,CODE_DELETE);
        dialogFragment.show(getFragmentManager(), BaseDialogFragment.TAG);
        deleted=dependency;
    }


    //Se llama al aceptar en el dialog para borrar la dependencia previamente guardada
    @Override
    public void onFinishDialog() {

        presenter.delete(deleted);
        deleted = null;
        presenter.load();
    }



    //*********************Metodos del contrato**************************


    @Override
    public void onStart() {
        super.onStart();
        presenter.load();
    }

    @Override
    public void showProgress() {
        llProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        llProgress.setVisibility(View.GONE);
    }

    @Override
    public void showNoData() {
        Log.d("BORRAR", "nodata");
        imgNoData.setVisibility(View.VISIBLE);
    }
    @Override
    public void showData()
    {
        imgNoData.setVisibility(View.GONE);
    }

    /**
     * Comprureba si hay datos en el fragment
     * @return
     */
    @Override
    public boolean hasData()
    {
        return imgNoData.getVisibility()==View.GONE;
    }

    /**
     * Actualiza adapter para mostrar datos
     * @param dependencyList
     */
    @Override
    public void showSuccess(List<Dependency> dependencyList) {
        adapter.clear();
        adapter.load(dependencyList);
        //se debe llamar al metodo notifyDataChanged para que se vuelva a cargar la vista

        adapter.notifyDataSetChanged();

    }


    @Override
    public void setPresenter(DependencyListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void onSuccess() {

    }
        //se ejecuta cuando el dato a sido eliminado
    @Override
    public void OnSuccessDeleted() {
        adapter.delete(deleted);
        adapter.notifyDataSetChanged();
        showSnackBarDeleted();
        deleted=null;
    }



    private void showSnackBarDeleted() {

        final Dependency undoDependency = deleted;
        Snackbar.make(getActivity().findViewById(android.R.id.content),"se ha eliminado",Snackbar.LENGTH_LONG).setAction(getString(R.string.undo), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                undoDeleted(undoDependency);
            }
        }).show();
    }

    private void undoDeleted(Dependency dependency) {
        presenter.undo(dependency);
    }

    @Override
    public void onSuccessUndo(Dependency dependency) {
        adapter.add(dependency);
    }

}
