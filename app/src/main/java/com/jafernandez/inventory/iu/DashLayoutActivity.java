package com.jafernandez.inventory.iu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.jafernandez.inventory.R;
import com.jafernandez.inventory.adapter.DependencyAdapter;
import com.jafernandez.inventory.iu.dependency.DependencyActivity;
import com.jafernandez.inventory.iu.seccion.SeccionActivity;


public class DashLayoutActivity extends AppCompatActivity  {

    private ImageButton btnDependency;
    private ImageButton btnSeccion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_layout);
        btnSeccion = findViewById(R.id.btnSectores);
        btnDependency = findViewById(R.id.btnDependencias);
        initilize();

    }

    private void initilize() {

        btnDependency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDependency();
            }
        });
        btnSeccion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startSeccion();
            }
        });
    }

    private void startDependency() {
        Intent intent = new Intent(DashLayoutActivity.this, DependencyActivity.class);
        startActivity(intent);
    }
    private void startSeccion()
    {
        Intent intent = new Intent(DashLayoutActivity.this, SeccionActivity.class);
        startActivity(intent);
    }


}
