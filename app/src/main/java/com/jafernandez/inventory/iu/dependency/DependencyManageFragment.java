package com.jafernandez.inventory.iu.dependency;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Parcel;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jafernandez.inventory.R;
import com.jafernandez.inventory.data.model.Dependency;

import java.util.Arrays;


public class DependencyManageFragment extends Fragment implements DependencyManageContract.View {

    public static final String ARG = "DependencyManageFragment";

    private FloatingActionButton fab;
    private EditText nombreCorto;
    private EditText nombre;
    private EditText Descripcion;
    private OnFragmentInteractionListener mListener;
    private Spinner spinner;
    Dependency dependency;

    private DependencyManageContract.Presenter presenter;

    public DependencyManageFragment() {
        // Required empty public constructor
    }

    public static DependencyManageFragment newInstance(Bundle bundle) {
        DependencyManageFragment fragment = new DependencyManageFragment();
        if (bundle!= null) {

            fragment.setArguments(bundle);

        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_dependency_add, container, false);
        fab = view.findViewById(R.id.fab);
        initFab();

        nombreCorto = view.findViewById(R.id.etNombreCorto);
        nombre = view.findViewById(R.id.etNombre);
        Descripcion = view.findViewById(R.id.etDescripcion);
        spinner = view.findViewById(R.id.spInventario);
        if(getArguments()!=null) {
            Bundle bundle = getArguments();
            dependency = bundle.getParcelable(Dependency.TAG);


            spinner.setSelection(Arrays.asList(getResources().getStringArray(R.array.spinventario)).indexOf(dependency.getInventory()));
            /*String inventory = dependency.getInventory();
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.spinventario, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            if (inventory != null) {
                int spinnerPosition = adapter.getPosition(inventory);
                spinner.setSelection(spinnerPosition);
            }*/

            nombreCorto.setText(dependency.getShortname());
            nombreCorto.setEnabled(false);
            nombre.setText(dependency.getName());
            Descripcion.setText(dependency.getDescription());
        }
        return view;

    }

    //este metodo valida la dependencia y bien añade o edita
    private void initFab() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidDependency()) {
                    presenter.validateDependency(getDependency());
                }
            }
        });

    }


    /**
     * metodo que comprueba las reglas de negocio del modelo
     * dependency
     * */
    private boolean isValidDependency() {
        //RND1:campos no vacios

        if(TextUtils.isEmpty(nombre.getText().toString()) ) {
            //muestro  el primer error
            Toast.makeText(getContext(), R.string.errNameEmpty, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(nombreCorto.getText().toString()) ) {
            Toast.makeText(getContext(), R.string.errShortNameEmpty, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(Descripcion.getText().toString()) ) {
            Toast.makeText(getContext(), R.string.errDescriptionEmpty, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private Dependency getDependency() {
        Dependency dependency = new Dependency();
        dependency.setName(nombre.getText().toString());
        dependency.setShortname(nombreCorto.getText().toString());
        dependency.setDescription(Descripcion.getText().toString());
        dependency.setInventory(spinner.getSelectedItem().toString());

        return dependency;

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (OnFragmentInteractionListener) context;
        } catch(ClassCastException e) {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /***************METODOS DEL CONTRATO DEPENDECY MANAGE CONTRACT*******************/
    @Override
    public void setPresenter(DependencyManageContract.Presenter presenter) {
    this.presenter = presenter;
    }

    @Override
    public void showError(String error) {

    }


    //metodo que es llamado desde el presentador despues de realizar una de las acciones
    //add/editar se debe mostrar el fragment anterior
    @Override
    public void onSuccess()
    {
        getActivity().onBackPressed();
    }


    //Metodo que es llamado desde el presentador despues de comprobar que a dependnecia es correcta
    @Override
    public void onSuccessValidate() {
        if(getArguments()!=null)
        {
            dependency = getDependency();
            presenter.edit(dependency);
        }
        else
        {
            dependency = getDependency();
            presenter.add(dependency);
        }
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
