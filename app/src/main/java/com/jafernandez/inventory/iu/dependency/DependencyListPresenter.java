package com.jafernandez.inventory.iu.dependency;

import android.os.AsyncTask;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.jafernandez.inventory.data.model.Dependency;
import com.jafernandez.inventory.data.repository.DependencyRepository;

import java.util.List;

public class DependencyListPresenter implements DependencyListContract.Presenter {

    DependencyListContract.View view;
    public DependencyListPresenter(DependencyListContract.View view)
    {
        this.view = view;
    }

    /**
     * eliminar no influye en los filtros en el presenter
     * Eliminar no influye en el orden
     * @param dependency
     */
    @Override
    public void delete(Dependency dependency) {

        //1. realizar la operacion en el Repositorio, y comprobar resultado.
        if(DependencyRepository.getInstance().delete(dependency))
        {
            //1.2 comprobar si no hay datos
            if(DependencyRepository.getInstance().getList().isEmpty())
                view.showNoData();

            view.OnSuccessDeleted();
        }
    }

    @Override
    public void undo(Dependency dependency) {
        if(DependencyRepository.getInstance().add(dependency))
        {
            view.onSuccessUndo(dependency);

        if(DependencyRepository.getInstance().getList().size()==1)
        {
            view.showData();
        }
        }
    }

    @Override
    public void load() {
        new LoadDataTask().execute();

      /*  new AsyncTask<Void, Void, List<Dependency>>()
        {

            @Override
            protected List<Dependency> doInBackground(Void... voids) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return DependencyRepository.getInstance().getList();
            }
        }.execute();*/
    }



    private class LoadDataTask extends AsyncTask<Void, Void, List<Dependency>>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showProgress();
        }

        @Override
        protected List<Dependency> doInBackground(Void... voids) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return DependencyRepository.getInstance().getList();
        }

        @Override
        protected void onPostExecute(List<Dependency> dependencies) {
            super.onPostExecute(dependencies);
                    view.hideProgress();
            if (dependencies.isEmpty())
            {
                Log.d("BORRAR", "estoy en isempty");
                view.showNoData();
            }
            else
            {
                if(!view.hasData()) {
                    Log.d("BORRAR", "estoy en hasdata");
                    view.showData();
                }

                view.showSuccess(dependencies);
            }
        }
    }

}
