package com.jafernandez.inventory.iu.seccion;

import com.jafernandez.inventory.data.model.Seccion;
import com.jafernandez.inventory.iu.base.BaseView;

public interface SeccionManageContract {
    interface View extends BaseView<Presenter>
    {
        void onSuccessValidate();
    }
    interface Presenter
    {
        void validateSeccion(Seccion seccion);
        void add(Seccion seccion);
        void edit(Seccion seccion);
    }

}
