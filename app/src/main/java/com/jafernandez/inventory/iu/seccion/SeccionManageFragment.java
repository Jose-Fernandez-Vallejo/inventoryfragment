package com.jafernandez.inventory.iu.seccion;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jafernandez.inventory.R;
import com.jafernandez.inventory.data.model.Seccion;


public class SeccionManageFragment extends Fragment implements SeccionManageContract.View {

    public static final String TAG = "SeccionManageFragment";
    private TextInputLayout tilShortName;
    private TextInputLayout tilName;
    private TextInputLayout tilDependency;
    private TextInputLayout tilDescription;
    SeccionManageContract.Presenter presenter;
    private FloatingActionButton fab;
    Seccion seccion;

    public SeccionManageFragment() {
        // Required empty public constructor
    }


    public static SeccionManageFragment newInstance(Bundle bundle) {
        SeccionManageFragment fragment = new SeccionManageFragment();

        if(bundle!=null) {
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_seccion_manage, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab = view.findViewById(R.id.fabSeccion);
        initfab();

        tilShortName = view.findViewById(R.id.tilShortNameSeccion);
        tilName = view.findViewById(R.id.tilNameSeccion);
        tilDependency = view.findViewById(R.id.tilDependencySeccion);
        tilDescription = view.findViewById(R.id.tilDescriptionSeccion);

        seccion = null;
        if(getArguments()!=null)
        {
            Bundle bundle = getArguments();

            seccion = bundle.getParcelable(Seccion.TAG);

            tilShortName.getEditText().setText(seccion.getShortName());
            tilName.getEditText().setText(seccion.getName());
            tilDependency.getEditText().setText(seccion.getDependency());
            tilDescription.getEditText().setText(seccion.getDescription());

            tilShortName.getEditText().setEnabled(false);

        }

    }

    private void initfab() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidSeccion())
                presenter.validateSeccion(getSeccion());
            }
        });
    }

    private boolean isValidSeccion() {
        if (tilShortName.getEditText().getText().toString().equals("")) {
            tilShortName.setError("el nombre corto esta vacio");
            return false;
        }
        tilShortName.setError(null);
        if (tilName.getEditText().getText().toString().equals("")) {
            tilName.setError("el nombre esta vacio");
            return false;
        }
        tilName.setError(null);
        if (tilDependency.getEditText().getText().toString().equals("")) {
            tilDependency.setError("la dependencia esta vacio");
            return false;
        }
        tilDependency.setError(null);
        if (tilDescription.getEditText().getText().toString().equals("")) {
            tilDescription.setError("la descripcion esta vacio");
            return false;
        }

        tilDescription.setError(null);
        return true;
    }

    @Override
    public void onSuccessValidate() {
        if(getArguments()!= null)
        {
            seccion = getSeccion();
            presenter.edit(seccion);
        }
        else
        {
            seccion = getSeccion();
            presenter.add(seccion);
        }

    }

    private Seccion getSeccion() {
        Seccion seccion1 = new Seccion();
        seccion1.setShortName(tilShortName.getEditText().getText().toString());
        seccion1.setName(tilName.getEditText().getText().toString());
        seccion1.setDependency(tilDependency.getEditText().getText().toString());
        seccion1.setDescription(tilDescription.getEditText().getText().toString());
        seccion1.setImage("");
        return seccion1;
    }

    @Override
    public void setPresenter(SeccionManageContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void onSuccess() {
        getActivity().onBackPressed();

    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/

}
