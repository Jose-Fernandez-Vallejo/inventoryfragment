package com.jafernandez.inventory.iu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jafernandez.inventory.R;
import com.jafernandez.inventory.utils.CommonUtils;

import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity {

        private Button btnSignUp;
        private TextInputEditText tieUser;
        private TextInputEditText tiePassword1;
        private TextInputEditText tiePassword2;
        private TextInputEditText tieEmail;

    private TextInputLayout tilUser;
    private TextInputLayout tilPassword1;
    private TextInputLayout tilPassword2;
    private TextInputLayout tilEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //recoge todas las cadenas introducidas
        tieUser = findViewById(R.id.tieUser);
        tiePassword1 = findViewById(R.id.tiePassword1);

        tieEmail = findViewById(R.id.tieEmail);


        tilUser = findViewById(R.id.tilUser);
        tilPassword1 = findViewById(R.id.tilPassword1);

        tilEmail = findViewById(R.id.tilEmail);
        tieUser.addTextChangedListener(new SignUpWatcher(tieUser));

        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
                validate();
            }
        });


    }
    //valida los campos de los layout
    private void validate()
    {
        if (validateUser(tieUser.getText().toString()) && validatePassword(tiePassword1.getText().toString() )&& validateEmail(tieEmail.getText().toString()))
        {
            //1. guardar usuario en BD
            //2. envio correo a usuario (Firebase)
            //3. Se pasa a la ventana Login

            finish();
        }
    }
    //valida email
    // no puede estar vacio
    private boolean validateUser(String user) {
        if (TextUtils.isEmpty(user))
        {
            tilUser.setError(getString(R.string.errUserEmpty));
            displaySoftKeyboard(tieUser);
            return false;
        }
        else{
            tilUser.setError(null);
            return true;
        }
    }




    /***
     * Este método valida las contraseñas:
     * 1. El tamaño es ocho, una mayúscula y un número
     * 2. No puede ser nulo
     * @return
     */
    private boolean validatePassword(String password1) {
        if (CommonUtils.CheckPassword(password1)) {
            tilPassword1.setError(null);
            return true;
        }
        tilPassword1.setError(getString(R.string.errPasswordWrong));
        displaySoftKeyboard(tiePassword1);
        return false;
    }

    /***
     * Este método valida que el email:
     * 1. No puede ser nulo
     * @param email
     * @return
     */
    private boolean validateEmail(String email) {
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            tilEmail.setError(null);
            return true;
        }
        tilEmail.setError(getString(R.string.errEmailWrong));
        displaySoftKeyboard(tieEmail);
        return false;
    }

    class SignUpWatcher implements TextWatcher
    {
        private View view;
        private SignUpWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.tieUser:{
                    validateUser(((TextInputEditText) view).getText().toString());
                    break;
                }
                case R.id.tiePassword1:{
                    validatePassword(((TextInputEditText) view).getText().toString());
                    break;
                }
                case R.id.tieEmail:{
                    validateEmail(((TextInputEditText) view).getText().toString());
                    break;
                }
            }
        }
    }

    /**
     * Este método abre el teclado en el caso que una vista tenga el foco (TextInputEditText) tenga el foco
     * @param view
     */
    private void displaySoftKeyboard(View view) {
        if (view.requestFocus()){
            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, 0);
        }
    }
}
